package com.tanhua.model.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "likevideo")
public class LikeVideo {
    private ObjectId id;
    private ObjectId publishId;    //动态id
    private Integer commentType;   //评论类型，1-点赞，2-评论，3-喜欢
    private Long userId;           //点赞人
    private Long commentUserId;    //被点赞人ID
    private Long created; 		   //发表时间
    private Integer likeCount = 0; //当前评论的点赞数
}
