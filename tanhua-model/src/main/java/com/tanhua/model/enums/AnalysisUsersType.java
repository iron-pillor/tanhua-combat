package com.tanhua.model.enums;

/**
 *  101 新增用户
 *  102 活跃用户
 *  103 次日留存率
 */
public enum AnalysisUsersType {

    REGISTERED(101), ACTIVE(102), RETENTIONID(103);

    Integer type;

    AnalysisUsersType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }
}
