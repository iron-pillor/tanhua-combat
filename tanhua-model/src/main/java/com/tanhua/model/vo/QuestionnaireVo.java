package com.tanhua.model.vo;

import com.tanhua.model.domain.Questions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireVo implements Serializable {
    private String id;
    private String name;
    private String cover;
    private String level;
    private Integer star;
    private List<QuestionsVo> questions;

    private Integer isLock;
    //是否锁住
    private String reportId;
   //报告id
}
