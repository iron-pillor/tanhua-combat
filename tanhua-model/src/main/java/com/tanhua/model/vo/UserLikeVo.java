package com.tanhua.model.vo;

import com.tanhua.model.domain.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLikeVo implements Serializable {
    private Integer id; //用户id
    private String avatar;//头像
    private String nickname;//昵称
    private String gender; //性别 man woman
    private Integer age;
    private String city;
    private String education;
    private Integer marriage;//婚姻状态
    private Integer matchRate;//匹配度
    private boolean alreadyLove;

    public static UserLikeVo init(UserInfo userInfo){
        UserLikeVo vo = new UserLikeVo();
        BeanUtils.copyProperties(userInfo,vo);
        return vo;
    }

}
