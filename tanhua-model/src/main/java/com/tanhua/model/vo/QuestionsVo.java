package com.tanhua.model.vo;

import com.tanhua.model.domain.Options;
import com.tanhua.model.domain.Questions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionsVo   implements Serializable {
    private String id;
    private String question;
    private List<OptionsVo> options;
}
