package com.tanhua.model.vo;

import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.Movement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovementMsgVo {
    private String id; //动态id
    private String nickname; //昵称
    private Integer userId; //用户id
    private String avatar; //头像
    private String createDate; //发布时间 如: 10分钟前


    private String textContent; //文字动态
    private String[] imageContent; //图片动态
    private Integer status;  //审核状态，1为待审核，2为自动审核通过，3为待人工审核，4为人工审核拒绝，5为人工审核通过，6为自动审核拒绝
    private Integer likeCount = 0; //点赞数
    private Integer commentCount = 0; //评论数

    public static MovementMsgVo init(UserInfo userInfo, Movement item) {
        MovementMsgVo vo = new MovementMsgVo();
        //设置动态数据
        BeanUtils.copyProperties(item, vo);
        vo.setId(item.getId().toHexString());
        //设置用户数据
        BeanUtils.copyProperties(userInfo, vo);
        //图片列表
        vo.setImageContent(item.getMedias().toArray(new String[]{}));

        Date date = new Date(item.getCreated());
        vo.setCreateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
        return vo;
    }
}
