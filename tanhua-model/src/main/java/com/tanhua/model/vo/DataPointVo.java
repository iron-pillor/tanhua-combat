package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataPointVo {
    /**
     * 日期
     */
    private String title;
    /**
     * 数量
     */
    private Long amount;
}