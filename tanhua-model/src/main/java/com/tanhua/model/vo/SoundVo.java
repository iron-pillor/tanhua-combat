package com.tanhua.model.vo;


import com.tanhua.model.domain.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoundVo extends UserInfo {
    private String soundUrl;
    private Integer remainingTimes;


}
