package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportVo implements Serializable {
    private String conclusion;
    private String cover;
    private ArrayList<Map<String, String>> dimensions;
    private ArrayList<Map<String, Object>> similarYou;

}
