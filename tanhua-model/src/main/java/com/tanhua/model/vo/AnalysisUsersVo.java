package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnalysisUsersVo {
    /**
     * 本年
     */
    private List<DataPointVo> thisYear;
    /**
     * 去年
     */
    private List<DataPointVo> lastYear;
}