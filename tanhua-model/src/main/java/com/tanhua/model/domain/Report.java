package com.tanhua.model.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_report")
public class Report implements Serializable {
    private Long id;

    /**
     * 鉴定结果
     */
    private String conclusion;

    /**
     * 鉴定图片url
     */
    private String cover;

    /**
     * 外向
     */
    private String extroversion;

    /**
     * 判断
     */
    private String judge;

    /**
     * 抽象
     */
    @TableField(value = "abstract")
    private String abstraction;

    /**
     * 理性
     */
    private String sense;

    /**
     * 测试用户id
     */
    private Long userId;

    /**
     * 创建时间
     */
    private Date created;

    /**
     * 问卷id
     */
    private Long questionnaireId;

    /**
     * 总分
     */
    private Long score;

}
