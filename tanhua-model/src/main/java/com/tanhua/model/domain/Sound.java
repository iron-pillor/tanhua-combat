package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sound implements Serializable {
    private Long id;
    private Long userId;
    private Long toUserId;
    private String soundUrl;
    private String sendTime;
    private String acceptTime;
}
