package com.tanhua.model.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_options")
public class Options implements Serializable {
    private Long id;
    private String elect;
    private Long score;
    private Long questionsId;
    private String dimensions;
}
