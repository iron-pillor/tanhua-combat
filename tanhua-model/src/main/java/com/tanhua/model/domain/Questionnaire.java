package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Questionnaire implements Serializable {
    private Long id;
    private String name;
    private String cover;
    private String level;
    private Integer star;
}
