package com.tanhua.admin.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tanhua.admin.exception.BusinessException;
import com.tanhua.admin.mapper.AnalysisMapper;
import com.tanhua.admin.mapper.LogMapper;
import com.tanhua.model.domain.Analysis;
import com.tanhua.model.enums.AnalysisUsersType;
import com.tanhua.model.vo.AnalysisUsersVo;
import com.tanhua.model.vo.DataPointVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class AnalysisService {

    @Autowired
    private LogMapper logMapper;

    @Autowired
    private AnalysisMapper analysisMapper;

    /**
     * 定时统计tb_log表中的数据，保存或者更新tb_analysis表
     *  1、查询tb_log表（注册用户数，登录用户数，活跃用户数，次日留存）
     *  2、构造Analysis对象
     *  3、保存或者更新
     */
    public void analysis() throws ParseException {
        //1、定义查询的日期
        String todayStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String yesdayStr = DateUtil.yesterday().toString("yyyy-MM-dd");
        //2、统计数据-注册数量
        Integer regCount = logMapper.queryByTypeAndLogTime("0102", todayStr);
        //3、统计数据-登录数量
        Integer loginCount = logMapper.queryByTypeAndLogTime("0101", todayStr);
        //4、统计数据-活跃数量
        Integer activeCount = logMapper.queryByLogTime(todayStr);
        //5、统计数据-次日留存
        Integer numRetention1d = logMapper.queryNumRetention1d(todayStr, yesdayStr);
        //6、构造Analysis对象
        //7、根据日期查询数据
        QueryWrapper<Analysis> qw = new QueryWrapper<Analysis>();
        qw.eq("record_date",new SimpleDateFormat("yyyy-MM-dd").parse(todayStr));
        Analysis analysis = analysisMapper.selectOne(qw);
        //8、如果存在，更新，如果不存在保存
        if(analysis != null) {
            analysis.setNumRegistered(regCount);
            analysis.setNumLogin(loginCount);
            analysis.setNumActive(activeCount);
            analysis.setNumRetention1d(numRetention1d);
            analysisMapper.updateById(analysis);
        }else {
            analysis = new Analysis();
            analysis.setNumRegistered(regCount);
            analysis.setNumLogin(loginCount);
            analysis.setNumActive(activeCount);
            analysis.setNumRetention1d(numRetention1d);
            analysis.setRecordDate(new SimpleDateFormat("yyyy-MM-dd").parse(todayStr));
            analysis.setCreated(new Date());
            analysis.setUpdated(new Date());
            analysisMapper.insert(analysis);
        }
    }

    /**
     * 查询指定日期的活跃用户
     */
    public Long queryActiveUserCount(Date ed, int i) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(ed);
        c.add(Calendar.DATE, +i);
        Date sd = c.getTime();

        //7、根据日期查询数据
        QueryWrapper<Analysis> qw = new QueryWrapper<Analysis>();
        qw.between("record_date",sd,ed);
        List<Analysis> analyses = analysisMapper.selectList(qw);
        if (analyses == null){
            return 0L;
        }
        int sum = 0;
        for (Analysis analysis : analyses) {
           sum += analysis.getNumActive();
        }
        return Long.valueOf(sum);
    }

    /**
     * 查询指定日期的新增用户
     */
    public Long queryRegisterUserCount(Date ed, int i) throws ParseException {

        Calendar c = Calendar.getInstance();
        c.setTime(ed);
        c.add(Calendar.DATE, +i);
        Date last = c.getTime();
        Date sd = DateUtil.date(last);
        //7、根据日期查询数据
        QueryWrapper<Analysis> qw = new QueryWrapper<Analysis>();
        qw.between("record_date",sd,ed);
        List<Analysis> analyses = analysisMapper.selectList(qw);
        if (analyses == null){
            return 0L;
        }
        int sum = 0;
        for (Analysis analysis : analyses) {
            sum += analysis.getNumRegistered();
        }
        return Long.valueOf(sum);
    }

    /**
     *查询指定日期的登录次数
     */
    public Long queryLoginUserCount(Date ed, int i) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(ed);
        c.add(Calendar.DATE, +i);
        Date sd = c.getTime();

        //7、根据日期查询数据
        QueryWrapper<Analysis> qw = new QueryWrapper<Analysis>();
        qw.between("record_date",sd,ed);
        List<Analysis> analyses = analysisMapper.selectList(qw);
        if (analyses == null){
            return 0L;
        }
        int sum = 0;
        for (Analysis analysis : analyses) {
            sum += analysis.getNumLogin();
        }
        return Long.valueOf(sum);

    }

    /**
     * 查询累计用户
     * @return
     */
    public long queryUserCount() {
        return analysisMapper.queryUserCount();
    }

    /**
     *  新增、活跃用户、次日留存率
     * @param sd    //开始时间
     * @param ed    //结束时间
     * @param type  //101 新增 102 活跃用户 103 次日留存率
     * @return
     */
    public AnalysisUsersVo queryAnalysisUsersVo(Long sd, Long ed, Integer type) {
        if (sd == null || ed == null || sd >= ed){
            throw new BusinessException("时间选择有误!");
        }
        Date startDate = new Date(sd);
        Date endDate = new Date(ed);
        //今年数据
        List<DataPointVo> thisYear = this.queryDataPointVo(startDate,endDate,type);
        //去年数据
        DateTime lsd = DateUtil.offsetMonth(startDate, -12);
        DateTime led = DateUtil.offsetMonth(endDate, -12);
        List<DataPointVo> lastYear = this.queryDataPointVo(lsd,led,type);

        AnalysisUsersVo analysisUsersVo = new AnalysisUsersVo();
        analysisUsersVo.setThisYear(thisYear);
        analysisUsersVo.setLastYear(lastYear);
        return analysisUsersVo;
    }

    private List<DataPointVo> queryDataPointVo(Date startDate, Date endDate, Integer type) {
        //查询这个时间段内的所有数据
        List<Map> list = new ArrayList<>();

        if (type != null && type.equals(AnalysisUsersType.REGISTERED.getType())){
            //新增用户
            list = analysisMapper.queryRegistered(startDate,endDate);
        }
        if (type != null && type.equals(AnalysisUsersType.ACTIVE.getType())){
            //活跃用户
            list = analysisMapper.queryActive(startDate,endDate);
        }
        if (type != null && type.equals(AnalysisUsersType.RETENTIONID.getType())){
            //次日留存率
            list = analysisMapper.queryRetentionId(startDate,endDate);
        }

        if (CollUtil.isEmpty(list)){
            return new ArrayList<>();
        }

        String title = "";
        Integer reAmount = 0;
        Long amount = 0L;

        List<DataPointVo> dataPointVos = new ArrayList<>();
        for (Map map : list) {
            if (CollUtil.isEmpty(map)){
                return new ArrayList<>();
            }
            DataPointVo dataPointVo = new DataPointVo();
            Date reTitle = (Date) map.get("record_date");
            title = new SimpleDateFormat("yyyy-MM-dd").format(reTitle);
            if (type != null && type.equals(AnalysisUsersType.REGISTERED.getType())){
                //新增用户
                reAmount = (Integer) map.get("num_registered");
            }
            if (type != null && type.equals(AnalysisUsersType.ACTIVE.getType())){
                //活跃用户
                reAmount = (Integer) map.get("num_active");
            }
            if (type != null && type.equals(AnalysisUsersType.RETENTIONID.getType())){
                //次日留存率
                reAmount = (Integer) map.get("num_retention1d");
            }

            amount = Long.valueOf(reAmount);
            dataPointVo.setTitle(title);
            dataPointVo.setAmount(amount);
            dataPointVos.add(dataPointVo);
        }

        return dataPointVos;
    }

    }
