package com.tanhua.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.Analysis;
import com.tanhua.model.vo.DataPointVo;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;


//累计用户
public interface AnalysisMapper extends BaseMapper<Analysis> {
    @Select("select sum(num_registered) from tb_analysis")
    Integer queryUserCount();

    //查询时间段内的新增用户
    @Select("SELECT record_date,num_registered FROM tb_analysis WHERE record_date BETWEEN #{startDate} AND #{endDate}")
    List<Map> queryRegistered(Date startDate, Date endDate);

    //查询时间段内的活跃用户
    @Select("SELECT record_date,num_active FROM tb_analysis WHERE record_date BETWEEN #{startDate} AND #{endDate}")
    List<Map> queryActive(Date startDate, Date endDate);

    //查询时间段内的活跃用户
    @Select("SELECT record_date,num_retention1d FROM tb_analysis WHERE record_date BETWEEN #{startDate} AND #{endDate}")
    List<Map> queryRetentionId(Date startDate, Date endDate);
}
