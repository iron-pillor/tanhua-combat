package com.tanhua.admin.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.tanhua.admin.service.AnalysisService;
import com.tanhua.admin.utils.ComputeUtil;
import com.tanhua.model.vo.AnalysisDistributionVo;
import com.tanhua.model.vo.AnalysisSummaryVo;
import com.tanhua.model.vo.AnalysisUsersVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@RestController
@RequestMapping("/dashboard")
@Slf4j
public class AnalysisController {

    @Autowired
    private AnalysisService analysisService;

    /**
     * 概要统计信息
     */
    @GetMapping("/summary")
    public AnalysisSummaryVo getSummary() throws Exception {
        AnalysisSummaryVo analysisSummaryVo = new AnalysisSummaryVo();
//        DateTime dateTime = DateUtil.date();
        Date date = DateUtil.date(new Date());
        //累计用户数
        analysisSummaryVo.setCumulativeUsers(Long.valueOf(this.analysisService.queryUserCount()));

        //过去7天活跃用户
        analysisSummaryVo.setActivePassWeek(this.analysisService.queryActiveUserCount(date, -8));

        //过去30天活跃用户
        analysisSummaryVo.setActivePassMonth(this.analysisService.queryActiveUserCount(date, -31));

        //今日新增用户
        analysisSummaryVo.setNewUsersToday(this.analysisService.queryRegisterUserCount(date, -1));
        //今日新增用户涨跌率，单位百分数，正数为涨，负数为跌
        Long current = this.analysisService.queryRegisterUserCount(date, -1);
        Long last = this.analysisService.queryRegisterUserCount(date, -2);
        analysisSummaryVo.setNewUsersTodayRate(ComputeUtil.computeRate(
                current,
                last
        ));

        //今日登录次数
        analysisSummaryVo.setLoginTimesToday(this.analysisService.queryLoginUserCount(date, -1));
        //今日登录次数涨跌率，单位百分数，正数为涨，负数为跌
        analysisSummaryVo.setLoginTimesTodayRate(ComputeUtil.computeRate(
                this.analysisService.queryLoginUserCount(date, -1),
                this.analysisService.queryLoginUserCount(date, -2)
        ));

        //今日活跃用户
        analysisSummaryVo.setActiveUsersToday(this.analysisService.queryActiveUserCount(date, -1));
        //今日活跃用户涨跌率，单位百分数，正数为涨，负数为跌
        analysisSummaryVo.setActiveUsersTodayRate(ComputeUtil.computeRate(
                this.analysisService.queryActiveUserCount(date,-1),
                this.analysisService.queryActiveUserCount(date,-2)
        ));

        return analysisSummaryVo;

    }


    /**
     *  新增、活跃用户、次日留存率
     * @param sd    //开始时间
     * @param ed    //结束时间
     * @param type  //101 新增 102 活跃用户 103 次日留存率
     * @return
     */
    @GetMapping("/users")
    public AnalysisUsersVo getUsers(@RequestParam(name = "sd") Long sd
            , @RequestParam("ed") Long ed
            , @RequestParam("type") Integer type) {
        return this.analysisService.queryAnalysisUsersVo(sd, ed, type);
    }

}