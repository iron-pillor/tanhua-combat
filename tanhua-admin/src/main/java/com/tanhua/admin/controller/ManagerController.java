package com.tanhua.admin.controller;

import com.tanhua.admin.service.ManagerService;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.vo.MovementMsgVo;
import com.tanhua.model.vo.PageResult;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/manage")
public class ManagerController {

    @Autowired
    private ManagerService managerService;


    @GetMapping("/users")
    public ResponseEntity users(@RequestParam(defaultValue = "1") Integer page,
                                @RequestParam(defaultValue = "10") Integer pagesize) {
        PageResult result = managerService.findAllUsers(page, pagesize);
        return ResponseEntity.ok(result);
    }

    //根据id查询
    @GetMapping("/users/{userId}")
    public ResponseEntity findUserById(@PathVariable("userId") Long userId) {
        UserInfo userInfo = managerService.findUserById(userId);
        return ResponseEntity.ok(userInfo);
    }

    /**
     * 查询指定用户发布的所有视频列表
     */
    @GetMapping("/videos")
    public ResponseEntity videos(@RequestParam(defaultValue = "1") Integer page,
                                 @RequestParam(defaultValue = "10") Integer pagesize,
                                 Long uid) {
        PageResult result = managerService.findAllVideos(page, pagesize, uid);
        return ResponseEntity.ok(result);
    }

    //查询动态
    @GetMapping("/messages")
    public ResponseEntity messages(@RequestParam(defaultValue = "1") Integer page,
                                   @RequestParam(defaultValue = "10") Integer pagesize,
                                   Long uid, Integer state) {
        PageResult result = managerService.findAllMovements(page, pagesize, uid, state);
        return ResponseEntity.ok(result);
    }

    //用户冻结
    @PostMapping("/users/freeze")
    public ResponseEntity freeze(@RequestBody Map params) {
        Map map = managerService.userFreeze(params);
        return ResponseEntity.ok(map);
    }

    //用户解冻
    @PostMapping("/users/unfreeze")
    public ResponseEntity unfreeze(@RequestBody Map params) {
        Map map = managerService.userUnfreeze(params);
        return ResponseEntity.ok(map);
    }

    /**
     * 动态详情
     * /manage/messages/:id
     */
    @GetMapping("/messages/{id}")
    public ResponseEntity movementMsg(@PathVariable("id") String movementId) {
        MovementMsgVo movementMsgVo = managerService.findMovementMsg(movementId);
        return ResponseEntity.ok(movementMsgVo);
    }

    /**
     * 动态通过
     * /manage/messages/pass
     */
    @PostMapping("/messages/pass")
    public ResponseEntity MsgPass(@RequestBody String[] ids) {
        Map map = managerService.msgPass(ids);
        return ResponseEntity.ok(map);
    }

    /**
     * 动态拒绝
     * /manage/messages/reject
     */
    @PostMapping("/messages/reject")
    public ResponseEntity MsgReject(@RequestBody String[] ids) {
        Map map = managerService.msgReject(ids);
        return ResponseEntity.ok(map);
    }

    /**
     * 评论列表翻页
     * /manage/messages/comments
     */
    @GetMapping("/messages/comments")
    public ResponseEntity commentMsg(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pagesize,
                                     String messageID) {
        PageResult pageResult = managerService.commentMsg(page, pagesize, messageID);
        return ResponseEntity.ok(pageResult);
    }
}
