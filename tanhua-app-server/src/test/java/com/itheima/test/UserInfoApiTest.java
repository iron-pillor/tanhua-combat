package com.itheima.test;

import com.tanhua.dubbo.api.ReportApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.model.domain.Report;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.server.AppServerApplication;
import org.apache.dubbo.config.annotation.DubboReference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppServerApplication.class)
public class UserInfoApiTest {

    @DubboReference
    private UserInfoApi userInfoApi;
    @Autowired
    RedisTemplate<String,String> redisTemplate;
    @Test
    public void test() {
        List ids = new ArrayList();
        ids.add(1l);
        ids.add(2l);
        ids.add(3l);
        ids.add(4l);
        UserInfo userInfo = new UserInfo();
        userInfo.setAge(35);
        Map map = userInfoApi.findByIds(ids, userInfo);
        map.forEach((k,v) -> System.out.println(k+"--"+v));
    }


    @DubboReference
    private ReportApi reportApi;
    @Test
    public void test1() {
     redisTemplate.opsForValue().set("一级","人们认为你是一个害羞的、神经质的、优柔寡断的，是须人照顾、永远要别人为你做决定、不想与任何事或任何人有关。他们认为你是一个杞人忧天者，一个永远看到不存在的问题的人。有些人认为你令人乏味，只有那些深知你的人知道你不是这样的人。");
     redisTemplate.opsForValue().set("二级","你的朋友认为你勤勉刻苦、很挑剔。他们认为你是一个谨慎的、十分小心的人，一个缓慢而稳定辛勤工作的人。如果你做任何冲动的事或无准备的事，你会令他们大吃一惊。他们认为你会从各个角度仔细地检查一切之后仍经常决定不做。他们认为对你的这种反应一部分是因为你的小心的`天性所引起的。");
     redisTemplate.opsForValue().set("三级","别人认为你是一个明智、谨慎、注重实效的人。也认为你是一个伶俐、有天赋有才干且谦虚的人。你不会很快、很容易和人成为朋友，但是是一个对朋友非常忠诚的人，同时要求朋友对你也有忠诚的回报。那些真正有机会了解你的人会知道要动摇你对朋友的信任是很难的，但相等的，一旦这信任被破坏，会使你很难熬过");
     redisTemplate.opsForValue().set("四级","　别人认为你是一个新鲜的、有活力的、有魅力的、好玩的、讲究实际的、而永远有趣的人;一个经常是群众注意力的焦点，但是你是一个足够平衡的人，不至於因此而昏了头。他们也认为你亲切、和蔼、体贴、能谅解人;一个永远会使人高兴起来并会帮助别人的人。");
    }
}
