package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.dubbo.api.*;
import com.tanhua.model.domain.*;

import com.tanhua.model.dto.Answers;
import com.tanhua.model.dto.SubmitDto;
import com.tanhua.model.vo.OptionsVo;
import com.tanhua.model.vo.QuestionnaireVo;
import com.tanhua.model.vo.QuestionsVo;
import com.tanhua.model.vo.ReportVo;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TestSoulService {
    @DubboReference
    private OptionsApi optionsApi;
    @DubboReference
    private QuestionnaireApi questionnaireApi;
    @DubboReference
    private QuestionsApi questionsApi;
    @DubboReference
    private ReportApi reportApi;
    @DubboReference
    private UserInfoApi userInfoApi;
    @Autowired
    private RedisTemplate<String, String> redis;

    public ResponseEntity find() {
        List<Questionnaire> questionnaireList = questionnaireApi.find();
        ArrayList<QuestionnaireVo> questionnaireVoArrayList = new ArrayList<>();
        for (Questionnaire questionnaire : questionnaireList) {

            List<Questions> questionsList = questionsApi.finds(questionnaire.getId());
            ArrayList<QuestionsVo> questionsVosList = new ArrayList<>();
            for (Questions questions : questionsList) {

                List<Options> optionsList = optionsApi.find(questions.getId());
                ArrayList<OptionsVo> optionsVoList = new ArrayList<>();
                for (Options options : optionsList) {
                    OptionsVo optionsVo = new OptionsVo();
                    optionsVo.setOption(options.getElect());
                    optionsVo.setId(options.getId() + "");
                    optionsVoList.add(optionsVo);
                }
                QuestionsVo questionsVo = new QuestionsVo();
                questionsVo.setQuestion(questions.getQuestion());
                questionsVo.setId(questions.getId() + "");
                questionsVo.setOptions(optionsVoList);
                questionsVosList.add(questionsVo);

            }
            QuestionnaireVo questionnaireVo = new QuestionnaireVo();
            questionnaireVo.setQuestions(questionsVosList);
            questionnaireVo.setId(questionnaire.getId() + "");
            BeanUtils.copyProperties(questionnaire, questionnaireVo);
            questionnaireVo.setIsLock(0);
            Report report = reportApi.findDate(UserHolder.getUserId(), questionnaire.getId());
            if (report != null) {
                questionnaireVo.setReportId(report.getId() + "");
            } else {
                questionnaireVo.setReportId(null);
            }

            questionnaireVoArrayList.add(questionnaireVo);
        }
        return ResponseEntity.ok(questionnaireVoArrayList);
    }

    public ResponseEntity result(Long id) {
        Report report = reportApi.findById(id);
        ReportVo reportVo = new ReportVo();
        reportVo.setCover(report.getCover());
        reportVo.setConclusion(report.getConclusion());
        HashMap<String, String> exreo = new HashMap<>();
        exreo.put("key", "外向");
        exreo.put("value", report.getExtroversion());
        HashMap<String, String> judge = new HashMap<>();
        judge.put("key", "判断");
        judge.put("value", report.getJudge());
        HashMap<String, String> abstraction = new HashMap<>();
        abstraction.put("key", "抽象");
        abstraction.put("value", report.getAbstraction());
        HashMap<String, String> sense = new HashMap<>();
        sense.put("key", "理性");
        sense.put("value", report.getSense());
        ArrayList<Map<String, String>> maps = new ArrayList<>();
        maps.add(exreo);
        maps.add(judge);
        maps.add(abstraction);
        maps.add(sense);
        reportVo.setDimensions(maps);
        Long score = report.getScore();
        List<Report> similarList = reportApi.findSimilar(score, UserHolder.getUserId());
        ArrayList<Map<String, Object>> userList = new ArrayList<>();
        if (similarList != null && similarList.size() != 0) {
            for (Report similarUser : similarList) {
                HashMap<String, Object> userMap = new HashMap<>();
                userMap.put("id", similarUser.getUserId());
                UserInfo info = userInfoApi.findById(similarUser.getUserId());
                userMap.put("avatar", info.getAvatar());
                userList.add(userMap);
            }
        } else {
            for (int i = 0; i < 1; i++) {
                HashMap<String, Object> userMap = new HashMap<>();
                userMap.put("id", 2L);
                userMap.put("avatar", "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_5.jpg");
                userList.add(userMap);
            }
        }
        reportVo.setSimilarYou(userList);
        return ResponseEntity.ok(reportVo);
    }


    /**
     * 提交问卷
     *
     * @param
     * @param
     * @return
     */
    public ResponseEntity submit(Answers answers) {
        ArrayList<SubmitDto> list = answers.getAnswers();
        SubmitDto submitDto1 = list.get(0);
        String questionId = submitDto1.getQuestionId();
       Long quId=  questionsApi.findById(questionId);
        Questionnaire questionnaire = questionnaireApi.findById(quId+"");
        ArrayList<Long> idList = new ArrayList<>();
        for (SubmitDto submitDto : list) {
            idList.add(Long.valueOf(submitDto.getOptionId()));
        }
        List<Options> optionsList = optionsApi.findAll(idList);
        Report report = new Report();
        //设置用户
        report.setUserId(UserHolder.getUserId());
        //设置试卷id
        report.setQuestionnaireId(questionnaire.getId());
        report.setCreated(new Date());
        Long count = 0L;
        Long extroversion = 0L;
        Long judge = 0L;
        Long abstraction = 0L;
        Long sense = 0L;
        for (Options options : optionsList) {
            String dimensions = options.getDimensions();
            if (dimensions.equals("extroversion")) {
                extroversion += options.getScore();
            } else if (dimensions.equals("sense")) {
                sense += options.getScore();
            } else if (dimensions.equals("abstraction")) {
                abstraction += options.getScore();
            } else {
                judge += options.getScore();
            }
            count += options.getScore();
        }
        report.setExtroversion(extroversion + 10+"%");
        report.setSense(sense +10+ "%");
        report.setAbstraction(abstraction +10+ "%");
        report.setJudge(judge + 10+"%");
        report.setScore(count);

        String conclusion = null;
        String cover = null;
        if (count >= 0 && count <= 30) {
            conclusion = redis.opsForValue().get("一级");
            cover ="https://tse1-mm.cn.bing.net/th/id/R-C.7c76f1ec0eb488bb8b1ca9d546b501df?rik=hpX5cGItMLIJHg&riu=http%3a%2f%2fwww.77cloub.com%2fuploads%2fimg1%2f20200807%2f8cc6eb6edf0d9e72728377058b72c2bd.jpg&ehk=FQruau6ujJToDlHABjMymYo7Dja8hobafybPZX%2fQan0%3d&risl=&pid=ImgRaw&r=0";
        } if (count > 30 && count <= 38) {
            conclusion = redis.opsForValue().get("二级");
            cover ="https://www.darenjiazu.com/uploads/allimg/200708/1603445A1-1.jpg";
        }
        if (count > 38 && count <= 45) {
            conclusion = redis.opsForValue().get("三级");
            cover ="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.hinews.cn%2Fpic%2F003%2F001%2F247%2F00300124776_818fc7eb.jpg&refer=http%3A%2F%2Fwww.hinews.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641973631&t=20b525f9421248b9b16e9361471e1785";
        } else {
            conclusion = redis.opsForValue().get("四级");
            cover ="https://gimg2.baidu.com/image_search/src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20190219%2Fdc4e734002404d079aab72f81972d44c.jpeg&refer=http%3A%2F%2F5b0988e595225.cdn.sohucs.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641974603&t=8223f3154258a5f6a8710b14bbe0d747";
        }
        report.setConclusion(conclusion);
        report.setCover(cover);
        Long id = reportApi.insert(report);
        return ResponseEntity.ok(id + "");
    }

}
