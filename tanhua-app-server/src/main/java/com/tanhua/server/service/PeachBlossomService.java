package com.tanhua.server.service;


import com.tanhua.autoconfig.template.OssTemplate;
import com.tanhua.dubbo.api.SoundApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.model.domain.Sound;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.model.vo.SoundVo;

import com.tanhua.server.exception.BusinessException;

import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * blp 探花传音
 */
@Service
public class PeachBlossomService {
    @DubboReference
    private SoundApi soundApi;
    @DubboReference
    private UserInfoApi userInfoApi;
  @Autowired
  private OssTemplate ossTemplate;
    public void save(MultipartFile soundFile) throws IOException {
        Integer count= soundApi.findByUserId(UserHolder.getUserId());
        if (count>10){
           throw new BusinessException(ErrorResult.chuanyin());
        }
        String upload = ossTemplate.upload(soundFile.getOriginalFilename(), soundFile.getInputStream());
        Sound sound = new Sound();
        sound.setSoundUrl(upload);
        sound.setUserId(UserHolder.getUserId());
        SimpleDateFormat dateFm = new SimpleDateFormat("yyyy-MM-dd"); //格式化当前系统日期
        String dateTime = dateFm.format(new Date());
        sound.setSendTime(dateTime);
        sound.setSoundUrl(upload);
        soundApi.save(sound);
    }

    public ResponseEntity accept() {
       Integer count= soundApi.findCount(UserHolder.getUserId());
       if (count>=10){
           throw new BusinessException(ErrorResult.jiehsou());
       }
       Sound sound=soundApi.accept(UserHolder.getUserId());
       if (sound==null){
           throw new BusinessException(ErrorResult.zanwu());
       }
        Long userId = sound.getUserId();
        UserInfo byId = userInfoApi.findById(userId);
        SoundVo soundVo = new SoundVo();
        BeanUtils.copyProperties(byId,soundVo);
        soundVo.setSoundUrl(sound.getSoundUrl());
        soundVo.setRemainingTimes(9-count);
        return ResponseEntity.ok(soundVo);
    }
}
