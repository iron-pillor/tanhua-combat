package com.tanhua.server.service;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.autoconfig.template.AipFaceTemplate;
import com.tanhua.autoconfig.template.OssTemplate;
import com.tanhua.dubbo.api.FriendApi;
import com.tanhua.dubbo.api.UserApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.dubbo.api.UserLikeApi;
import com.tanhua.model.domain.User;
import com.tanhua.model.domain.UserInfo;


import com.tanhua.model.mongo.Friend;
import com.tanhua.model.mongo.UserLike;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.vo.*;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;
import com.tanhua.model.domain.User;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class UserInfoService {

    @DubboReference
    private UserInfoApi userInfoApi;

    @DubboReference
    private UserLikeApi userLikeApi;

    @DubboReference
    private FriendApi friendApi;

    @Autowired
    private OssTemplate ossTemplate;

    @Autowired
    private AipFaceTemplate aipFaceTemplate;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @DubboReference
    private UserApi userApi;


    public void save(UserInfo userInfo) {
        userInfoApi.save(userInfo);
    }

    //更新用户头像
    public void updateHead(MultipartFile headPhoto, Long id) throws IOException {
        //1、将图片上传到阿里云oss
        String imageUrl = ossTemplate.upload(headPhoto.getOriginalFilename(), headPhoto.getInputStream());
        //2、调用百度云判断是否包含人脸
        boolean detect = aipFaceTemplate.detect(imageUrl);
        //2.1 如果不包含人脸，抛出异常
        if(!detect) {
            throw new BusinessException(ErrorResult.faceError());
        }else{
            //2.2 包含人脸，调用API更新
            UserInfo userInfo = new UserInfo();
            userInfo.setId(id);
            userInfo.setAvatar(imageUrl);
            userInfoApi.update(userInfo);
        }
    }

    //根据id查寻
    public UserInfoVo findById(Long id) {
        UserInfo userInfo = userInfoApi.findById(id);

        UserInfoVo vo = new UserInfoVo();

        BeanUtils.copyProperties(userInfo,vo); //copy同名同类型的属性

        if(userInfo.getAge() != null) {
            vo.setAge(userInfo.getAge().toString());
        }

        return vo;
    }

    //更新
    public void update(UserInfo userInfo) {
        userInfoApi.update(userInfo);
    }

    public void sendVerificationCode(UserInfo userInfo) {

    }
     public void sendMsg(String phone) {
        String code = "123456";
        if (redisTemplate.hasKey("CHECK_CODE_" + phone)) {
            throw new RuntimeException("验证码还未失效");
        }
//        smsTemplate.sendSms(phone,code);
        redisTemplate.opsForValue().set("CHECK_CODE_"+phone,code, Duration.ofMinutes(5));
    }

    public Boolean checkCode(String phone, String code) {
        String redisCode = (String) redisTemplate.opsForValue().get("CHECK_CODE_" + phone);
        //对验证码进行校验
        if(StringUtils.isEmpty(redisCode) || !redisCode.equals(code)) {
//            throw new RuntimeException("验证码错误");
            return false;
        }
        redisTemplate.delete("CHECK_CODE_" + phone);
        return true;
    }

    public void newPhone(String phone, Long userId) {
//        UserInfo info = userInfoApi.findById(userId);
        User user = userApi.findById(userId);
        user.setMobile(phone);
        userApi.update(user);
        UserHolder.set(user);
    }

    /*    public List<UserInfo> queryUserInfoList(QueryWrapper<UserInfo> queryWrapper) {
            return this.userInfoMapper.selectList(queryWrapper);
        }*/
//相互喜欢，喜欢和粉丝数据的统计
    public CountsVo counts(Long userID) {
        return userLikeApi.counts(userID);
    }

    //    客户端互相喜欢用户列表查询功能
    public PageResult eachLove(Integer page, Integer pagesize, Long userID, String nickname) {
        List<Friend> friends = userLikeApi.eachLove(userID);
        List<UserLikeVo> items = new ArrayList<>();

        for (Friend friend : friends) {
            UserLikeVo userLikeVo = new UserLikeVo();
            UserInfo userInfo = userInfoApi.findById1(friend.getFriendId(), nickname);
            if (userInfo != null) {
                give(userLikeVo, userInfo);
                //是否喜欢
                userLikeVo.setAlreadyLove(true);
                items.add(userLikeVo);
            }
        }

        Long counts = Long.valueOf(items.size());
        PageResult pr = new PageResult(counts, pagesize, (counts + pagesize - 1) / pagesize, page, items);
        return pr;
    }

    //    客户端喜欢用户列表查询功能
    public PageResult love(Integer page, Integer pagesize, Long userID, String nickname) {
        List<UserLike> likes = userLikeApi.like(userID);
        List<UserLikeVo> items = new ArrayList<>();
        for (UserLike like : likes) {
            UserLikeVo userLikeVo = new UserLikeVo();
            UserInfo userInfo = userInfoApi.findById1(like.getLikeUserId(),nickname);
            if(userInfo!= null){
                give(userLikeVo, userInfo);
                userLikeVo.setAlreadyLove(like.getIsLike());
                items.add(userLikeVo);
            }
        }
        Long counts = Long.valueOf(items.size());
        PageResult pr = new PageResult(counts, pagesize, (counts + pagesize - 1) / pagesize, page, items);
        return pr;
    }
    //    粉丝列表查询功能
    public PageResult fans(Integer page, Integer pagesize, Long userID, String nickname) {
        List<UserLike> fanLikes = userLikeApi.fans(userID);
        List<UserLikeVo> items = new ArrayList<>();
        for (UserLike fanLike : fanLikes) {
            UserLikeVo userLikeVo = new UserLikeVo();
            UserInfo userInfo = userInfoApi.findById1(fanLike.getUserId(),nickname);

            if(userInfo!=null){
                give(userLikeVo, userInfo);
                if (friendApi.findById(fanLike.getUserId(), fanLike.getLikeUserId())) {
                    userLikeVo.setAlreadyLove(true);
                } else {
                    userLikeVo.setAlreadyLove(false);
                }

                items.add(userLikeVo);
            }

        }
        Long counts =Long.valueOf(items.size());
        PageResult pr = new PageResult(page, pagesize, counts, items);
        return pr;
    }

    //粉丝喜欢
    public void fansLike(Long uid) {
        Long userId = UserHolder.getUserId();
        Boolean re = userLikeApi.saveOrUpdate(userId, uid, true);
        if (!re) {
            throw new BusinessException(ErrorResult.loveError());
        }
        friendApi.save(userId, uid);


    }

    //查看谁看过我
    public PageResult late(Integer page, Integer pagesize, Long userID, String nickname) {
        List<Video> list = userLikeApi.lates(userID);
        Long counts = userLikeApi.latesCounts(userID);
        ArrayList<UserLikeVo> items = new ArrayList<>();
        for (Video video : list) {
            UserInfo userinfo = userInfoApi.findById(video.getVid());
            UserLikeVo userLikeVo = UserLikeVo.init(userinfo);
            userLikeVo.setMatchRate((new Random()).nextInt(70) + 30);
            userLikeVo.setAlreadyLove(true);
            items.add(userLikeVo);
        }

        PageResult result = new PageResult(counts, pagesize, (counts + pagesize - 1) / pagesize, page, items);
        return result;
    }

    //给 vo赋值
    private void give(UserLikeVo userLikeVo, UserInfo userInfo) {
        String s = userInfo.getId().toString();
        Integer userId = Integer.valueOf(s);
        userLikeVo.setId(userId);
        userLikeVo.setAvatar(userInfo.getAvatar());
        userLikeVo.setNickname(userInfo.getNickname());
        userLikeVo.setGender(userInfo.getGender());
        userLikeVo.setAge(userInfo.getAge());
        userLikeVo.setCity(userInfo.getCity());
        userLikeVo.setEducation(userInfo.getEducation());
        userLikeVo.setMarriage(userInfo.getMarriage());
        userLikeVo.setMatchRate((new Random()).nextInt(70) + 30);
    }

    //取消喜欢
    public void fansDislike(Long uid) {
        Long userId = UserHolder.getUserId();
        Boolean re = userLikeApi.saveOrUpdate(userId, uid, false);
        Boolean re1 = friendApi.removeFindByid(userId, uid);

        if (!re) {
            throw new BusinessException(ErrorResult.disloveError());
        }

    }
}
