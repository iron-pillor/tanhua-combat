package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tanhua.autoconfig.template.HuanXinTemplate;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.*;
import com.tanhua.model.domain.Announcement;
import com.tanhua.model.domain.User;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.enums.CommentType;
import com.tanhua.model.mongo.Comment;
import com.tanhua.model.mongo.Friend;
import com.tanhua.model.mongo.Movement;
import com.tanhua.model.vo.*;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MessagesService {

    @DubboReference
    private UserApi userApi;

    @DubboReference
    private UserInfoApi userInfoApi;

    @DubboReference
    private FriendApi friendApi;

    @DubboReference
    private CommentApi commentApi;

    @DubboReference
    private MovementApi movementApi;

    @Autowired
    private HuanXinTemplate huanXinTemplate;



    @DubboReference
    private AnnouncementApi announcementApi;

    /**
     * 根据环信id查询用户详情
     */
    public UserInfoVo findUserInfoByHuanxin(String huanxinId) {
        //1、根据环信id查询用户
        User user = userApi.findByHuanxin(huanxinId);
        //2、根据用户id查询用户详情
        UserInfo userInfo = userInfoApi.findById(user.getId());
        UserInfoVo vo = new UserInfoVo();
        BeanUtils.copyProperties(userInfo,vo); //copy同名同类型的属性
        if(userInfo.getAge() != null) {
            vo.setAge(userInfo.getAge().toString());
        }
        return vo;
    }

    //添加好友关系
    public void contacts(Long friendId) {
        //1、将好友关系注册到环信
        Boolean aBoolean = huanXinTemplate.addContact(Constants.HX_USER_PREFIX + UserHolder.getUserId(),
                Constants.HX_USER_PREFIX + friendId);
        if(!aBoolean) {
            throw new BusinessException(ErrorResult.error());
        }
        //2、如果注册成功，记录好友关系到mongodb
        friendApi.save(UserHolder.getUserId(),friendId);
    }

    //分页查询联系人列表
    public PageResult findFriends(Integer page, Integer pagesize, String keyword) {
        //1、调用API查询当前用户的好友数据 -- List<Friend>
        List<Friend> list = friendApi.findByUserId(UserHolder.getUserId(),page,pagesize);
        if(CollUtil.isEmpty(list)) {
            return new PageResult();
        }
        //2、提取数据列表中的好友id
        List<Long> userIds = CollUtil.getFieldValues(list, "friendId", Long.class);
        //3、调用UserInfoAPI查询好友的用户详情
        UserInfo info = new UserInfo();
        info.setNickname(keyword);
        Map<Long, UserInfo> map = userInfoApi.findByIds(userIds, info);
        //4、构造VO对象
        List<ContactVo> vos = new ArrayList<>();
        for (Friend friend : list) {
            UserInfo userInfo = map.get(friend.getFriendId());
            if(userInfo != null) {
                ContactVo vo = ContactVo.init(userInfo);
                vos.add(vo);
            }
        }
        return new PageResult(page,pagesize,0l,vos);
    }

    /**
     * 查询点赞列表
     * @param page
     * @param pagesize
     * @param commentType
     * @return
     */
    public PageResult findlikes(Integer page, Integer pagesize, Integer commentType) {
        //获取当前登录的用户Id
        Long userId = UserHolder.getUserId();
        //根据userid获取当评论的
        List<Comment> list = commentApi.findByUserId(userId, page, pagesize, commentType);
        ArrayList<Map<String, String>> maps = new ArrayList<>();
        for (Comment comment : list) {
            UserInfo byId = userInfoApi.findById(comment.getUserId());
            HashMap<String, String> map = new HashMap<>();
            map.put("id",comment.getId().toString());
            map.put("avatar",byId.getAvatar());
            map.put("nickname", byId.getNickname());

            SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );
            Date d= new Date(comment.getCreated());
            String str = sdf.format(d);

            map.put("createDate",str);
            maps.add(map);
        }
        return new PageResult(page,pagesize, (long) list.size(),maps);
    }


    //公告列表
    public PageResult findannouncements(Integer page, Integer pageSize) {
        Page ipage = announcementApi.announcements(page,pageSize);
        if (ipage == null){
            return new PageResult();
        }
        List<AnnouncementVo> vos = new ArrayList<>();
        List<Announcement> records = ipage.getRecords();
        for (Announcement record : records) {
            AnnouncementVo vo = new AnnouncementVo();
            vo.setId(record.getId().toString());
            vo.setTitle(record.getTitle());
            vo.setDescription(record.getDescription());
            SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );
            Date created = record.getCreated();
            String str = sdf.format(created);
            vo.setCreateDate(str);
            vos.add(vo);
        }
        return new PageResult(page,pageSize,ipage.getTotal(),vos);
    }

}
