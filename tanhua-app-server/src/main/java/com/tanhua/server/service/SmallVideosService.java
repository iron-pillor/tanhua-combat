package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.PageUtil;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.tanhua.autoconfig.template.OssTemplate;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.dubbo.api.VideoApi;
import com.tanhua.dubbo.api.VideoReviewApi;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.mongo.VideoReview;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.VideoCommentVo;
import com.tanhua.model.vo.VideoVo;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SmallVideosService {

    @Autowired
    private FastFileStorageClient client;

    @Autowired
    private FdfsWebServer webServer;

    @Autowired
    private OssTemplate ossTemplate;

    @DubboReference
    private VideoApi videoApi;

    @DubboReference
    private VideoReviewApi videoReviewApi;

    @DubboReference
    private UserInfoApi userInfoApi;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private MqMessageService mqMessageService;

    /**
     * 上传视频
     * @param videoThumbnail 封面图片文件
     * @param videoFile  视频文件
     */
    public void saveVideos(MultipartFile videoThumbnail, MultipartFile videoFile) throws IOException {

        if(videoFile.isEmpty() || videoThumbnail.isEmpty()) {
            throw new BusinessException(ErrorResult.error());
        }

        //1、将视频上传到FastDFS,获取访问URL
        String filename = videoFile.getOriginalFilename();  // abc.mp4
        filename = filename.substring(filename.lastIndexOf(".")+1);
        StorePath storePath = client.uploadFile(videoFile.getInputStream(), videoFile.getSize(), filename, null);
        String videoUrl = webServer.getWebServerUrl() + storePath.getFullPath();
        //2、将封面图片上传到阿里云OSS，获取访问的URL
        String imageUrl = ossTemplate.upload(videoThumbnail.getOriginalFilename(), videoThumbnail.getInputStream());
        //3、构建Videos对象
        Video video = new Video();
        video.setUserId(UserHolder.getUserId());
        video.setPicUrl(imageUrl);
        video.setVideoUrl(videoUrl);
        video.setText("我就是我，不一样的烟火");
        //4、调用API保存数据
        String videoId = videoApi.save(video);
        if(StringUtils.isEmpty(videoId)) {
            throw new BusinessException(ErrorResult.error());
        }

        //发送消息
        mqMessageService.sendLogMessage(UserHolder.getUserId(),"0301","video",videoId);
    }

    //查询视频列表
    @Cacheable(
            value="videos",
            key = "T(com.tanhua.server.interceptor.UserHolder).getUserId()+'_'+#page+'_'+#pagesize")  //userid _ page_pagesize
    public PageResult queryVideoList(Integer page, Integer pagesize) {

        //1、查询redis数据
        String redisKey = Constants.VIDEOS_RECOMMEND +UserHolder.getUserId();
        String redisValue = redisTemplate.opsForValue().get(redisKey);
        //2、判断redis数据是否存在，判断redis中数据是否满足本次分页条数
        List<Video> list = new ArrayList<>();
        int redisPages = 0;
        if(!StringUtils.isEmpty(redisValue)) {
            //3、如果redis数据存在，根据VID查询数据
            String[] values = redisValue.split(",");
            //判断当前页的起始条数是否小于数组总数
            if( (page -1) * pagesize < values.length) {
                List<Long> vids = Arrays.stream(values).skip((page - 1) * pagesize).limit(pagesize)
                        .map(e->Long.valueOf(e))
                        .collect(Collectors.toList());
                //5、调用API根据PID数组查询动态数据
                list = videoApi.findMovementsByVids(vids);
            }
            redisPages = PageUtil.totalPage(values.length,pagesize);
        }
        //4、如果redis数据不存在，分页查询视频数据
        if(list.isEmpty()) {
            //page的计算规则，  传入的页码  -- redis查询的总页数
            list = videoApi.queryVideoList(page - redisPages, pagesize);  //page=1 ?
        }
        //5、提取视频列表中所有的用户id
        List<Long> userIds = CollUtil.getFieldValues(list, "userId", Long.class);
        //6、查询用户信息
        Map<Long, UserInfo> map = userInfoApi.findByIds(userIds, null);
        //7、构建返回值
        List<VideoVo> vos = new ArrayList<>();
        for (Video video : list) {
            UserInfo info = map.get(video.getUserId());
            if(info != null) {
                VideoVo vo = VideoVo.init(info, video);
                String guanzhu = (String) redisTemplate.opsForHash().get("guanzhu" + UserHolder.getUserId(), info.getId() + "");
                if (guanzhu != null) {
                    vo.setHasFocus(1);
                }
                vos.add(vo);
            }
        }

        return new PageResult(page,pagesize,0l,vos);
    }




    //视频`评论列表
    public ResponseEntity comments(String id, Integer page, Integer pagesize) {
        //1评论
   List<VideoReview>list= videoReviewApi.findComments(id,1,page,pagesize);
        //2、判断list集合是否存在
        if(CollUtil.isEmpty(list)) {
            return ResponseEntity.ok( new PageResult());
        }
        //3、提取所有的用户id,调用UserInfoAPI查询用户详情
        List<Long> userIds = CollUtil.getFieldValues(list, "userId", Long.class);
        Map<Long, UserInfo> map = userInfoApi.findByIds(userIds, null);
        //4、构造vo对象
        List<VideoCommentVo> vos = new ArrayList<>();
        for (VideoReview VideoRev : list) {
            UserInfo userInfo = map.get(VideoRev.getUserId());
            if(userInfo != null) {
                VideoCommentVo vo = VideoCommentVo.init(userInfo,VideoRev);

                String likeVideo = (String) redisTemplate.opsForHash().get("likeVideo"+UserHolder.getUserId().toString(), VideoRev.getId().toString());
                if (likeVideo!=null){
                    vo.setHasLiked(1);
                }

                vos.add(vo);
            }
        }
        //5、构造返回值
        return ResponseEntity.ok(new PageResult(page,pagesize,0l,vos));
    }

    //发布评论
    public void saveComments(String movementId, String comment) {
        //1、获取操作用户id
        Long userId = UserHolder.getUserId();
        //2、构造Comment
        VideoReview comment1 = new VideoReview();
        comment1.setVideoId(new ObjectId(movementId));
       // redisTemplate.opsForHash().put(movementId,UserHolder.getUserId(),"1");
        comment1.setVideoType(1);
        comment1.setContent(comment);
        comment1.setUserId(userId);
        comment1.setCreated(System.currentTimeMillis());
        //3、调用API保存评论
        Integer commentCount = videoReviewApi.save(comment1);

    }



    @CacheEvict(value="videos",allEntries = true)
    public void focusOn(Long id,Integer type) {
        if (type==1){
            redisTemplate.opsForHash().put("guanzhu"+UserHolder.getUserId(),id+"","1");
        }else{
            redisTemplate.opsForHash().delete("guanzhu"+UserHolder.getUserId(),id+"");
        }
    }

    //视频点赞
    public Long likeComment(String videoId) {
        Boolean result = this.videoApi.likeComment(UserHolder.getUserId(), videoId);
        if (result) {
            return this.videoApi.queryLikeCount(videoId);
        }
        return null;
    }
    //视频取消点赞
    public Long disLikeComment(String videoId) {
        Boolean result = this.videoApi.disLikeComment(UserHolder.getUserId(), videoId);
        if (result) {
            return this.videoApi.queryLikeCount(videoId);
        }
        return null;
    }


    @CacheEvict(value="videos",allEntries = true)
    public void likeVideo(String id, int type) {
        if (type==1){
            redisTemplate.opsForHash().put("likeVideo"+UserHolder.getUserId(),id+"","1");
            videoReviewApi.update(id,type);
        }else{
            redisTemplate.opsForHash().delete("likeVideo"+UserHolder.getUserId(),id+"");
            videoReviewApi.update(id,type);
        }
    }
}
