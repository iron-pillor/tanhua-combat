package com.tanhua.server.controller;

import com.tanhua.model.dto.Answers;

import com.tanhua.server.service.TestSoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/testSoul")
public class TestSoulController {
    @Autowired
    private TestSoulService testSoulService;

    @GetMapping()
    public ResponseEntity testSoul() {
        return testSoulService.find();
    }
    //查看结果
    @GetMapping("report/{id}")
    public ResponseEntity result(@PathVariable("id")Long id){
       return testSoulService.result(id);
    }
    /**
     * 提交问卷
     */
    @PostMapping
    public ResponseEntity submit(@RequestBody Answers answers){
   return   testSoulService.submit(answers);

    }
}
