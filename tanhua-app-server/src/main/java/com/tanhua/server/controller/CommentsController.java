package com.tanhua.server.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.tanhua.model.mongo.Comment;
import com.tanhua.model.vo.PageResult;
import com.tanhua.server.service.CommentsService;
import org.checkerframework.checker.index.qual.GTENegativeOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/comments")
public class CommentsController {

    @Autowired
    private CommentsService commentsService;

    /**
     * 发布评论
     */
    @PostMapping
    public ResponseEntity saveComments(@RequestBody Map map) {
        String movementId = (String )map.get("movementId");
        String comment = (String)map.get("comment");
        commentsService.saveComments(movementId,comment);
        return ResponseEntity.ok(null);
    }

    //分页查询评论列表
    @GetMapping
    public ResponseEntity findComments(@RequestParam(defaultValue = "1") Integer page,
                                    @RequestParam(defaultValue = "10") Integer pagesize,
                                       String movementId) {
        PageResult pr = commentsService.findComments(movementId,page,pagesize);
        return ResponseEntity.ok(pr);
    }

    /**
     *评论点赞
     * @param commentId  评论的id
     * @return
     */
    @GetMapping("{id}/like")
    public ResponseEntity commentsLike(@PathVariable("id") String commentId){
        Integer likeCount = commentsService.commentsLike(commentId);
        return ResponseEntity.ok(likeCount);
    }

     /**
     * 取消评论点赞
     * @param commentId 评论的id
     * @return
     */
    @GetMapping("{id}/dislike")
    public ResponseEntity commentsDisLike(@PathVariable("id") String commentId){
        Integer likeCount = commentsService.commentsDisLike(commentId);
        return ResponseEntity.ok(likeCount);
    }
}
