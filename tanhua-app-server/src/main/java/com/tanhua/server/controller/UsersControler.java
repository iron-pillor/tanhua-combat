package com.tanhua.server.controller;

import com.tanhua.dubbo.api.UserLikeApi;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.vo.CountsVo;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.UserInfoVo;
import com.tanhua.model.vo.UserLikeVo;
import com.tanhua.server.interceptor.UserHolder;
import com.tanhua.server.service.UserInfoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/users")
public class UsersControler {

    @Autowired
    private UserInfoService userInfoService;

    @DubboReference
    private UserLikeApi userLikeApi;

    /**
     * 查询用户资料
     */
    @GetMapping
    public ResponseEntity users(Long userID) {
        if(userID == null) {
            userID = UserHolder.getUserId();
        }
        UserInfoVo userInfo = userInfoService.findById(userID);
        return ResponseEntity.ok(userInfo);
    }

    /**
     * 更新用户资料
     */
    @PutMapping
    public ResponseEntity updateUserInfo(@RequestBody UserInfo userInfo) {
        userInfo.setId(UserHolder.getUserId());
        userInfoService.update(userInfo);
        return ResponseEntity.ok(null);
    }

    @PostMapping("/header")
    public ResponseEntity updateHead( MultipartFile headPhoto) throws IOException {
        userInfoService.updateHead(headPhoto,UserHolder.getUserId());
        return ResponseEntity.ok(null);

    }
    @PostMapping("/phone/sendVerificationCode")
    public ResponseEntity senMsg(){
        Long userId = UserHolder.getUserId();
        String phone = UserHolder.getMobile();
        userInfoService.sendMsg(phone);
        return ResponseEntity.ok(null);
    }
    @PostMapping("/phone/checkVerificationCode")
    public Map<String,Boolean> checkCode(@RequestBody Map map){
//        String code = "123456";
        String code = (String) map.get("verificationCode");
        String phone = UserHolder.getMobile();
        Map<String,Boolean> mapVo = new HashMap<>();
        Boolean checkCode = userInfoService.checkCode(phone, code);
        mapVo.put("verification",checkCode);
        return mapVo;

    }
    @PostMapping("/phone")
    public  ResponseEntity newPhone(@RequestBody Map map){
        Long userId = UserHolder.getUserId();
        String phone = (String) map.get("phone");
        userInfoService.newPhone(phone,userId);
        return ResponseEntity.ok(null);
    }
    /*
     * 相互喜欢，喜欢和粉丝数据的统计
     * */
    @GetMapping("/counts")
    public ResponseEntity counts(Long userID) {
        if (userID == null) {
            userID = UserHolder.getUserId();
        }
        CountsVo vo = userInfoService.counts(userID);

        return ResponseEntity.ok(vo);
    }

    //客户端互相喜欢、喜欢、粉丝、谁看过我的用户列表查询功能
    @GetMapping("/friends/{type}")
    public ResponseEntity friendType(@RequestParam(defaultValue = "1") Integer page,
                                     @RequestParam(defaultValue = "10") Integer pagesize,
                                     @PathVariable Integer type,
                                     String nickname) {

        Long userID = UserHolder.getUserId();
        if (type == 1) {
            PageResult result = userInfoService.eachLove(page, pagesize, userID, nickname);
            return ResponseEntity.ok(result);
        } else if (type == 2) {
            PageResult result = userInfoService.love(page, pagesize, userID, nickname);
            return ResponseEntity.ok(result);
        } else if (type == 3) {
            PageResult result = userInfoService.fans(page, pagesize, userID, nickname);
            return ResponseEntity.ok(result);
        } else {
            PageResult result = userInfoService.late(page, pagesize, userID, nickname);
            return ResponseEntity.ok(result);
        }

    }


    //    粉丝喜欢
    @PostMapping("/fans/{uid}")
    public ResponseEntity fansLike(@PathVariable Long uid) {
        userInfoService.fansLike(uid);
        return ResponseEntity.ok(null);
    }

    //    取消喜欢
    @DeleteMapping("/like/{uid}")
    public ResponseEntity notLike(@PathVariable Long uid){
        userInfoService.fansDislike(uid);
        return ResponseEntity.ok(null);
    }
}
