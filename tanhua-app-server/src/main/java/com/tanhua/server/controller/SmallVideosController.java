package com.tanhua.server.controller;

import com.tanhua.model.vo.PageResult;
import com.tanhua.server.service.SmallVideosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.Path;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/smallVideos")
public class SmallVideosController {

    @Autowired
    private SmallVideosService videosService;

    /**
     * 发布视频
     * 接口路径：POST
     * 请求参数：
     * videoThumbnail：封面图
     * videoFile：视频文件
     */
    @PostMapping
    public ResponseEntity saveVideos(MultipartFile videoThumbnail, MultipartFile videoFile) throws IOException {
        videosService.saveVideos(videoThumbnail, videoFile);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频列表
     */
    @GetMapping
    public ResponseEntity queryVideoList(@RequestParam(defaultValue = "1") Integer page,
                                         @RequestParam(defaultValue = "10") Integer pagesize) {
        PageResult result = videosService.queryVideoList(page, pagesize);
        return ResponseEntity.ok(result);
    }

    /**
     * 关注和取消关注
     * @param id
     * @return
     */
    @PostMapping("/{id}/userFocus")
    public ResponseEntity userUnFocus(@PathVariable("id") Long id){
        videosService.focusOn(id,1);
        return ResponseEntity.ok(null);
    }


    @PostMapping("/{id}/userUnFocus")
    public ResponseEntity userFocus(@PathVariable("id") Long id){
        videosService.focusOn(id,2);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频的点赞
     * @param videoId
     * @return
     */
    @PostMapping("/{id}/like")

    public ResponseEntity<Long> likeComment(@PathVariable("id") String videoId) {
        try {
            Long likeCount = this.videosService.likeComment(videoId);
            if (likeCount != null) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }


    /**
     * 视频取消点赞
     * @param videoId
     * @return
     */
    @PostMapping("/{id}/dislike")

    public ResponseEntity<Long> disLikeComment(@PathVariable("id") String videoId) {
        try {
            Long likeCount = this.videosService.disLikeComment(videoId);
            if (null != likeCount) {
                return ResponseEntity.ok(likeCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }




    @GetMapping("/{id}/comments")
    public ResponseEntity comments(@PathVariable("id") String id, Integer page, Integer pagesize) {
        return videosService.comments(id, page, pagesize);
    }


    /**
     * 发布评论
     */
    @PostMapping("{id}/comments")
    public ResponseEntity saveComments(@PathVariable("id") String videoId, @RequestBody Map<String, String> map) {
        String comment = map.get("comment");
        videosService.saveComments(videoId, comment);
        return ResponseEntity.ok(null);
    }


    /**
     * 视频评论的点赞
     * @param id
     * @return
     */
    @PostMapping("/comments/{id}/like")
    public ResponseEntity like(@PathVariable("id") String id) {
        videosService.likeVideo(id,1);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频评论的取消点赞
     * @param id
     * @return
     */
    @PostMapping("/comments/{id}/dislike")
    public ResponseEntity dislike(@PathVariable("id") String id) {
        videosService.likeVideo(id,2);
        return ResponseEntity.ok(null);
    }
}