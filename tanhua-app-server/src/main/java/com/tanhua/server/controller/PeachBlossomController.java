package com.tanhua.server.controller;

import com.tanhua.server.service.PeachBlossomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/peachblossom")
public class PeachBlossomController {
    @Autowired
    private PeachBlossomService peachBlossomService;

    @PostMapping
    public ResponseEntity PeachBlossom(MultipartFile soundFile) throws IOException {
          peachBlossomService.save(soundFile);
          return ResponseEntity.ok(null);
    }

    @GetMapping
    public ResponseEntity accept(){
       return peachBlossomService.accept();
    }
}
