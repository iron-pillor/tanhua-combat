package com.tanhua.server.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.tanhua.model.enums.CommentType;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.UserInfoVo;
import com.tanhua.server.service.MessagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/messages")
public class MessagesController {

    @Autowired
    private MessagesService messagesService;

    /**
     * 根据环信Id查询用户详情
     * @param huanxinId
     * @return
     */
    @GetMapping("/userinfo")
    public ResponseEntity userinfo(String huanxinId) {
       UserInfoVo vo = messagesService.findUserInfoByHuanxin(huanxinId);
        return ResponseEntity.ok(vo);
    }

    /**
     * 添加好友
     */
    @PostMapping("/contacts")
    public ResponseEntity contacts(@RequestBody Map map) {
        Long friendId = Long.valueOf(map.get("userId").toString());
        messagesService.contacts(friendId);
        return ResponseEntity.ok(null);
    }

    /**
     * 分页查询联系人列表
     */
    @GetMapping("/contacts")
    public ResponseEntity contacts(@RequestParam(defaultValue = "1") Integer page,
                                   @RequestParam(defaultValue = "10") Integer pagesize,
                                   String keyword) {
        PageResult pr = messagesService.findFriends(page,pagesize,keyword);
        return ResponseEntity.ok(pr);
    }

    /**
     * 给我动态点赞的用户分页查询
     * @param page
     * @param pagesize
     * @param commentType
     * @return
     */
    @GetMapping("/likes")
    public ResponseEntity likes (@RequestBody
                                 @RequestParam(defaultValue = "1") Integer page,
                                 @RequestParam(defaultValue = "10") Integer pagesize,
                                 CommentType commentType){
        PageResult pr = messagesService.findlikes(page,pagesize,1);
        return ResponseEntity.ok(pr);
    }

    /**
     * 给我动态喜欢的用户分页查询
     * @param page
     * @param pagesize
     * @param commentType
     * @return
     */
    @GetMapping("/loves")
    public ResponseEntity loves (@RequestParam(defaultValue = "1") Integer page,
                                 @RequestParam(defaultValue = "10") Integer pagesize,
                                 CommentType commentType){
        PageResult pr = messagesService.findlikes(page,pagesize,3);
        return ResponseEntity.ok(pr);
    }

    /**
     * 给我的动态评论的用户分页查询
     * @param page
     * @param pagesize
     * @param commentType
     * @return
     */
    @GetMapping("/comments")
    public ResponseEntity comments (@RequestParam(defaultValue = "1") Integer page,
                                    @RequestParam(defaultValue = "10") Integer pagesize,
                                    CommentType commentType){
        PageResult pr = messagesService.findlikes(page,pagesize,2);
        return ResponseEntity.ok(pr);
    }

    /**
     * 系统公告的分页查询
     * @param page
     * @param pagesize
     * @param
     * @return
     */
    @GetMapping("/announcements")
    public ResponseEntity announcements (@RequestParam(defaultValue = "1") Integer page,
                                         @RequestParam(defaultValue = "10") Integer pagesize
                                         ){
        PageResult pr = messagesService.findannouncements(page,pagesize);
        return ResponseEntity.ok(pr);
    }

}
