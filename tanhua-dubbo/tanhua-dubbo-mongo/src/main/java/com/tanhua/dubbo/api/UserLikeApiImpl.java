package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.Friend;
import com.tanhua.model.mongo.UserLike;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.vo.CountsVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

@DubboService
public class UserLikeApiImpl implements UserLikeApi{

    @Autowired
    private MongoTemplate mongoTemplate;



    @Override
    public Boolean saveOrUpdate(Long userId, Long likeUserId, boolean isLike) {
        try {
            //1、查询数据
            Query query = Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(likeUserId));
            UserLike userLike = mongoTemplate.findOne(query, UserLike.class);
            //2、如果不存在，保存
            if(userLike == null) {
                userLike = new UserLike();
                userLike.setUserId(userId);
                userLike.setLikeUserId(likeUserId);
                userLike.setCreated(System.currentTimeMillis());
                userLike.setUpdated(System.currentTimeMillis());
                userLike.setIsLike(isLike);
                mongoTemplate.save(userLike);
            }else {
                //3、更新
                Update update = Update.update("isLike", isLike)
                        .set("updated",System.currentTimeMillis());
                mongoTemplate.updateFirst(query,update,UserLike.class);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //查询互相喜欢，喜欢，粉丝总数
    @Override
    public CountsVo counts(Long userID) {
        Query query = Query.query(Criteria.where("userId").is(userID));
        Integer eachLoveCount = Math.toIntExact(mongoTemplate.count(query, Friend.class));

        Query query1 = Query.query(Criteria.where("userId").is(userID).
                and("isLike").is(true));
        Integer loveCount = Math.toIntExact(mongoTemplate.count(query1, UserLike.class));

        Query query2 = Query.query(Criteria.where("likeUserId").is(userID).
                and("isLike").is(true));
        Integer fanCount = Math.toIntExact(mongoTemplate.count(query2, UserLike.class));
        CountsVo vo = new CountsVo();
        vo.setEachLoveCount(eachLoveCount);
        vo.setLoveCount(loveCount);
        vo.setFanCount(fanCount);
        return vo;
    }

    //分页查询互相喜欢
    @Override
    public List<Friend> eachLove(Long userID) {
        Query query = Query.query(Criteria.where("userId").is(userID));
        List<Friend> friends = mongoTemplate.find(query, Friend.class);
        return friends;

    }

    //查看喜欢的人
    @Override
    public List<UserLike> like( Long userID) {

        Query query = Query.query(Criteria.where("userId").is(userID).
                and("isLike").is(true));

        List<UserLike> likes = mongoTemplate.find(query, UserLike.class);
        return likes;
    }

    //查看粉丝
    @Override
    public List<UserLike> fans( Long userID) {
        Query query = Query.query(Criteria.where("likeUserId").is(userID).
                and("isLike").is(true));
        List<UserLike> fanslikes = mongoTemplate.find(query, UserLike.class);
        return fanslikes;
    }

    //谁看过我
    @Override
    public List<Video> lates(Long userID) {

        Query query = Query.query(Criteria.where("userId").is(userID));
        return mongoTemplate.find(query, Video.class);
    }

    @Override
    public Long latesCounts(Long userID) {
        Query query=Query.query(Criteria.where("userId").is(userID));
        return mongoTemplate.count(query,Video.class);
    }


}
