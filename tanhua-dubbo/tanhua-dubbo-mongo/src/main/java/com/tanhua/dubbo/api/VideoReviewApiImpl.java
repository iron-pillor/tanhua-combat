package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.Video;
import com.tanhua.model.mongo.VideoReview;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

@DubboService
public class VideoReviewApiImpl implements VideoReviewApi{
    @Autowired
    private MongoTemplate mongoTemplate;

    //分页查询
    public List<VideoReview> findComments(String id, Integer type, Integer page, Integer pagesize) {
        //1、构造查询条件
        Query query = Query.query(Criteria.where("videoId").is( new ObjectId(id)).and("VideoType")
                .is(type))
                .skip((page -1) * pagesize)
                .limit(pagesize)
                .with(Sort.by(Sort.Order.desc("created")));
        //2、查询并返回
        return mongoTemplate.find(query,VideoReview.class);
    }

    //发布评论
    @Override
    public Integer save(VideoReview comment1) {
        //1、查询动态
        Video video = mongoTemplate.findById(comment1.getVideoId(), Video.class);
        //2、向comment对象设置被评论人属性
        if(video != null) {
            comment1.setPublishUserId(video.getUserId());
        }
        //3、保存到数据库
        mongoTemplate.save(comment1);
        //4、更新动态表中的对应字段
        Query query = Query.query(Criteria.where("id").is(comment1.getVideoId()));
        Update update = new Update();
        if(comment1.getVideoType()== 2) {
            update.inc("likeCount",1);
        }else if (comment1.getVideoType() == 1){
            update.inc("commentCount",1);
        }else {
            update.inc("loveCount",3);
        }
        //设置更新参数
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true) ;//获取更新后的最新数据
        Video modify = mongoTemplate.findAndModify(query, update, options, Video.class);
        //5、获取最新的评论数量，并返回
        return modify.getCommentCount()+1;
    }

    @Override
    public void update(String id, int i) {
        Update update = new Update();
        Query id1 = new Query(Criteria.where("_id").is(new ObjectId(id)));
        if (i==1){
            update.inc("likeCount",+1);
        }else {
            update.inc("likeCount",-1);
        }

        VideoReview andModify = mongoTemplate.findAndModify(id1, update, VideoReview.class);
      //  return
    }


}
