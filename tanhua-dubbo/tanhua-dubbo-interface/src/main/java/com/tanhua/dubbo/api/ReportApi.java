package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Report;

import java.util.List;

public interface ReportApi {
    Report findById(long l);

    Report findDate(Long userId,Long shijuan);

    List<Report> findSimilar(Long score,Long userid);

    Long insert(Report report);

}
