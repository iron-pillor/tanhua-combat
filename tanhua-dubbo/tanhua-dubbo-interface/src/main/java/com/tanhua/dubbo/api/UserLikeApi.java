package com.tanhua.dubbo.api;



import com.tanhua.model.mongo.Friend;
import com.tanhua.model.mongo.UserLike;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.vo.CountsVo;


import java.util.List;

public interface UserLikeApi {

    //保存或者更新
    Boolean saveOrUpdate(Long userId, Long likeUserId, boolean isLike);

    CountsVo counts(Long userID);

    //互相喜欢
    List<Friend> eachLove(Long userId);

    List<UserLike> like(Long userID);

    List<UserLike> fans( Long userID);

    List<Video> lates(Long userID);

    Long latesCounts(Long userID);
}
