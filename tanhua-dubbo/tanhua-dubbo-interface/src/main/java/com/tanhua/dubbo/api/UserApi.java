package com.tanhua.dubbo.api;

import com.tanhua.model.domain.User;

import java.util.List;

public interface UserApi {

    //根据手机号码查询用户
    User findByMobile(String mobile);

    //保存用户，返回用户id
    Long save(User user);

    //更新
    void update(User user);

    //根据id查询
    User findById(Long userId);
    
    List<User> findAll();


    //根据环信id查询用户
    User findByHuanxin(String huanxinId);
}
