package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Questions;

import java.util.List;
import java.util.Map;

public interface QuestionsApi {
    List<Questions> finds(Long id);

    Long findById(String questionId);
}
