package com.tanhua.dubbo.api;


import com.tanhua.model.domain.Sound;

public interface SoundApi {
    void save(Sound sound);


    Integer findByUserId(Long userId);

    Integer findCount(Long userId);


    Sound accept(Long userId);

}
