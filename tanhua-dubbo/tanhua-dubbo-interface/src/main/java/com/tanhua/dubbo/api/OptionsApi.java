package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Options;

import java.util.ArrayList;
import java.util.List;

public interface OptionsApi {
    List<Options> find(Long id);

    List<Options> findAll(ArrayList<Long> idList);

}
