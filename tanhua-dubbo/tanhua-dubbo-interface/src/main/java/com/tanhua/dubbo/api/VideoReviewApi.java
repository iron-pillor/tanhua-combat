package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.VideoReview;

import java.util.List;

public interface VideoReviewApi {
    public List<VideoReview> findComments(String id, Integer type, Integer page, Integer pagesize);


    Integer save(VideoReview comment1);

    void update(String id, int i);
}
