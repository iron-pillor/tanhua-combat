package com.tanhua.dubbo.api;

import com.tanhua.model.enums.CommentType;
import com.tanhua.model.mongo.Comment;
import com.tanhua.model.mongo.CommentLike;
import com.tanhua.model.vo.PageResult;

import java.util.List;

public interface CommentApi {

    //发布评论，并获取评论数量
    Integer save(Comment comment1);

    //分页查询
    PageResult findComments(String movementId, CommentType comment, Integer page, Integer pagesize);

    //判断comment数据是否存在
    Boolean hasComment(String movementId, Long userId, CommentType like);

    //删除comment数据
    Integer delete(Comment comment);

    //获取评论点赞数量
    Integer saveCount(CommentLike commentLike);
    //判断评论是否点赞
    Boolean hasCommentLike(String commentId, Long userId, CommentType like);
    //取消点赞
    Integer deletelike(CommentLike commentLikes);

    //给我点赞的用户列表
    List findByUserId(Long userId, Integer page, Integer pagesize , Integer commentType);
}
