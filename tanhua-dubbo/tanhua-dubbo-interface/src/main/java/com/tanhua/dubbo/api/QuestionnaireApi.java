package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Questionnaire;

import java.util.List;

public interface QuestionnaireApi {
    List<Questionnaire> find();


    Questionnaire findById(String questionId);

}
