package com.tanhua.dubbo.api;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.mappers.QuestionsMapper;
import com.tanhua.model.domain.Questions;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DubboService
public class QuestionsApiImpl implements QuestionsApi{
    @Autowired
    private QuestionsMapper questionsMapper;
    @Override
    public List<Questions> finds(Long id) {
        QueryWrapper<Questions> query = new QueryWrapper<>();
        query.eq("questionnaire_id",id);
        List<Questions> questions = questionsMapper.selectList(query);
       return questions;
    }

    @Override
    public Long findById(String questionId) {
        Questions questions = questionsMapper.selectById(Long.valueOf(questionId));
       return questions.getQuestionnaireId();
    }
}
