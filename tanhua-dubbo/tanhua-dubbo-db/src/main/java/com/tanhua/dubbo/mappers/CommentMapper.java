package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.mongo.Comment;

public interface CommentMapper extends BaseMapper<Comment> {
}
