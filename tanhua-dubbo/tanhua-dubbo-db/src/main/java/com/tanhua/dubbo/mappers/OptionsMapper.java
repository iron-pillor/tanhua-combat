package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OptionsMapper extends BaseMapper<Options> {
    @Select("select * from tb_options where questions_id=#{id}")
    List<Options> findById(Long id);
}
