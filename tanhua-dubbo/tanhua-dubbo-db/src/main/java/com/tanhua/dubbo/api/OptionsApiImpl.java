package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.mappers.OptionsMapper;
import com.tanhua.model.domain.Options;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class OptionsApiImpl implements OptionsApi{
    @Autowired
    private OptionsMapper mapper;
    @Override
    public List<Options> find(Long id) {
     return mapper.findById(id);
    }

    @Override
    public List<Options> findAll(ArrayList<Long> idList) {

        QueryWrapper<Options> query = new QueryWrapper<>();
        query.in("id",idList);
       return mapper.selectList(query);
    }
}
