package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.Questionnaire;

public interface QuestionnaireMapper  extends BaseMapper<Questionnaire> {
}
