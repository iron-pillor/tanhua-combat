package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tanhua.dubbo.mappers.AnnouncementMapper;
import com.tanhua.model.domain.Announcement;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
@DubboService
public class AnnouncementApiImpl implements AnnouncementApi{
    @Autowired
    private AnnouncementMapper announcementMapper;


    @Override
    public Page announcements(Integer page, Integer pageSize) {
        Page ipage = new Page(page, pageSize);
        QueryWrapper<Announcement> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("updated");
        Page page1 = announcementMapper.selectPage(ipage, wrapper);
        return page1;
    }
}
