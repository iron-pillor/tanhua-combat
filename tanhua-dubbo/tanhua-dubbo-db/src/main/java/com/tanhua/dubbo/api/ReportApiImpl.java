package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tanhua.dubbo.mappers.ReportMapper;
import com.tanhua.model.domain.Report;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class ReportApiImpl extends ServiceImpl<ReportMapper,Report> implements ReportApi {
    @Autowired
    private ReportMapper reportMapper;

    @Override
    public Report findById(long l) {
        Report report = reportMapper.selectById(l);
        return report;
    }

    //根据id查询报告id 并返回最新的
    @Override
    public Report findDate(Long userId,Long shijuan) {
        QueryWrapper<Report> query = new QueryWrapper<>();
        query.eq("user_id",userId);
        query.eq("questionnaire_id",shijuan);
        query.orderByDesc("created");
        List<Report> reports = reportMapper.selectList(query);
        if (reports!=null&& reports.size()!=0){
            return reports.get(0);
        }
        return null;
    }

    @Override
    public List<Report> findSimilar(Long score,Long id) {
        QueryWrapper<Report> query = new QueryWrapper<>();
        query.between("score",score-20,score+20);
        query.ne("user_id",id);
        List<Report> reports = reportMapper.selectList(query);
        if (reports==null|| reports.isEmpty()){
            return null;
        }
        ArrayList<Report> reportArrayList = new ArrayList<>();
        reportArrayList.add(reports.get(0));
        for (Report report : reports) {
            Long userId = report.getUserId();
            for (int i = 0; i < reportArrayList.size(); i++) {
                Report report1 = reportArrayList.get(i);
                if (userId!=report1.getUserId()){
                    reportArrayList.add(report);
                }
            }
        }
        return reportArrayList;
    }

    @Override
    public Long insert(Report report) {
        save(report);
        return report.getId();
    }
}
