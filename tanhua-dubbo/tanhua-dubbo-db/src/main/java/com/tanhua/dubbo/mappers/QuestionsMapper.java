package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.Questions;

public interface QuestionsMapper extends BaseMapper<Questions> {
}
