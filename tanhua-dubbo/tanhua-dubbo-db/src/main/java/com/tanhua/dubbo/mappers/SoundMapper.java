package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.tanhua.model.domain.Sound;

public interface SoundMapper extends BaseMapper<Sound> {
}
