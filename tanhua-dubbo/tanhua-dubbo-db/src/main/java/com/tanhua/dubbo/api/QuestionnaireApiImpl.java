package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.mappers.QuestionnaireMapper;
import com.tanhua.model.domain.Questionnaire;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DubboService
public class QuestionnaireApiImpl implements QuestionnaireApi{
    @Autowired
    private QuestionnaireMapper mapper;


    @Override
    public List<Questionnaire> find() {
        List<Questionnaire> questionnaireList = mapper.selectList(new QueryWrapper<Questionnaire>());
        return questionnaireList;
    }

    @Override
    public Questionnaire findById(String questionId) {
       return mapper.selectById(Long.valueOf(questionId));
    }
}
