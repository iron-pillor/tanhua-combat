package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.tanhua.dubbo.mappers.SoundMapper;

import com.tanhua.model.domain.Sound;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@DubboService
public class SoundApiImpl implements SoundApi {

    @Autowired
    private SoundMapper soundMapper;

    @Override
    public void save(Sound sound) {
        soundMapper.insert(sound);
    }

    @Override
    public Integer findByUserId(Long userId) {
        SimpleDateFormat dateFm = new SimpleDateFormat("yyyy-MM-dd"); //格式化当前系统日期
        String dateTime = dateFm.format(new Date());
        QueryWrapper<Sound> query = new QueryWrapper<>();
        query.eq("user_id", userId);
        query.eq("send_time", dateTime);
        Integer integer = soundMapper.selectCount(query);
        return integer;
    }

    @Override
    public Integer findCount(Long userId) {
        QueryWrapper<Sound> query = new QueryWrapper<>();
        query.eq("to_user_id", userId);
        SimpleDateFormat dateFm = new SimpleDateFormat("yyyy-MM-dd"); //格式化当前系统日期
        String dateTime = dateFm.format(new Date());
        query.eq("accept_time", dateTime);
        Integer integer = soundMapper.selectCount(query);
        return integer;
    }

    @Override
    public Sound accept(Long userId) {
        QueryWrapper<Sound> query = new QueryWrapper<>();

        List<Sound> sounds = soundMapper.selectList(query);
        ArrayList<Sound> sounds1 = new ArrayList<>();
        for (Sound sound : sounds) {
            if (sound.getUserId() != userId) {
                if (sound.getToUserId() == null) {
                    sounds1.add(sound);
                }
            }

        }
        if (sounds1.isEmpty()) {
            return null;
        }
        int i = new Random().nextInt(sounds1.toArray().length);
        Sound sound = sounds1.get(i);
        sound.setToUserId(userId);
        SimpleDateFormat dateFm = new SimpleDateFormat("yyyy-MM-dd"); //格式化当前系统日期
        String dateTime = dateFm.format(new Date());
        sound.setAcceptTime(dateTime);
        soundMapper.updateById(sound);
        return sound;
    }


}
